# ngDemo #

This is a demo project for AngularJS workshops. Here you can find an example of very basic AngularJS app along with some additional stuff.

### How to run the demo app? ###

This app requires no DB, but you still need a server in order to run it locally since AngularJS loads external templates with ajax requests. You can use any kind of web server you like, but I'd recommend [this](https://www.npmjs.com/package/http-server) tiny http server, which is really easy to install and use.
